from flask import flask

app = Flask(__name__)

@app.route('/')
def hello_world():
    return "Hello, app"

if __name__ == '__main__':
    print('Hello, Console!')
    i = 10
    while i > 0:
        print(f'Times {i}')
        i -= 1